# path to source code repository
path=.

# colors
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # default

# path to VERSION file
file="${path}/VERSION"
env="${path}/.env"
login=$(sed -n '2p' ${env})
passwd=$(sed -n '3p' ${env})

echo "Obtaining remote version from GitLab..."
remote_version=$(curl https://gitlab.com/rknadmin/backend-version/-/raw/main/VERSION?ref_type=heads)
local_version=$(head -n 1 $file)
echo
echo "Local version:" ${local_version}
echo "Remote version: " ${remote_version}

if [[ ${local_version} != ${remote_version} ]] ; then
    echo "${RED}Versions different, updating Docker image...${NC}"
    cd ${path}
    echo ${passwd} | docker login registry.gitlab.com -u ${login} --password-stdin
    docker-compose pull && docker-compose up -d
    echo ${remote_version} > ${file}
else
    echo "${GREEN}Already up-to-date${NC}"
fi
